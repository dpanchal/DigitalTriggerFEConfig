/*

Ltdb.h

Header for Ltdb.cpp
Author: D.Panchal
Email: dpanchal@utexas.edu
Date created: April 27, 2020

*/

#ifndef __LTDB_H__
#define __LTDB_H__

# define LOG(X) Log(__PRETTY_FUNCTION__, X)

#include <iostream>
#include <memory>

#include <Base/Core/OnlineObject.h>
#include "LTDB/ClientSession.h"

#include <LTDBSegment/LargDal/LARG_LTDB_HW.h>
#include <LTDBSegment/LargDal/LARG_LTDB_Server.h>

class Ltdb : public Larg::OnlineObject {
    public:
        Ltdb(void);
        Ltdb(const uint32_t, const std::string&);
        ~Ltdb();

        Ltdb(const Ltdb&) = delete;
        Ltdb& operator=(const Ltdb&) = delete;

        ////////////////////// Setter methods //////////////////////
        void setSerialNumber(const std::string&);
        void setServerAddress(const std::string&);
        void setNodeId(const std::string&);
        void setMaxAttempts(const uint32_t);

        ////////////////////// Getter methods //////////////////////
        const std::string getSerialNumber();
        const std::string getServerAddress();
        const std::string getNodeId();

        ////////////////// TDAQ FSM implementations ////////////////
        virtual bool Init_();
        virtual bool Load_();
        virtual bool Unload_();
        virtual bool Config_();
        virtual bool PrepareForRun_();
        virtual bool Unconfig_();
        virtual bool Start_();
        virtual bool Stop_();

        bool DefineFromOKS(const Larg::dal::LARG_LTDB_HW*&); 
        void Log(const std::string&, const std::string& = "");

    private:
        std::string m_serverAddress;
        std::string m_nodeId;
        std::string m_serialNumber;
        uint32_t maxRetries;
        ClientSession* m_client;

        static const unsigned int NUM_CONNECTION_RETRIES;
        static const unsigned int WAIT_BETWEEN_RETRIES;
};

#endif