#ifndef __CLIENTSESSION_H__
#define __CLIENTSESSION_H__

#include <iostream>
#include <string>
#include <uaclient/uasession.h>
#include "LTDB/UaoExceptions.h"


class ClientSession
{
    public:
        ClientSession();
        ~ClientSession();

        bool connect(const UaString& serverAddress);
        
        bool tryConnect(const UaString& serverAddress, 
                        unsigned int numRetries,
                        unsigned int waitBetweenRetry);
        
        bool disconnect();
        
        bool IsConnected();
        
        bool call(const char* nodeToConfigure,
                  const char* function,
                  unsigned int maxRetries);
        
        bool write(const char* nodeToWrite,
                   const char* functionToWrite,
                   const bool data);
        
        UaDataValues readData(const char* nodeToRead,
                              const char* function,
                              UaStatus *out_status);
        
        std::string readName(const char* nodeToRead);
        
        uint32_t readInt32(const char* nodeToRead,
                           const char* function);
        
        bool readBoolean(const char* nodeToRead,
                         const char* function);

        uint8_t readByte(const char* nodeToRead,
                         const char* function);
    private:
        UaClientSdk::UaSession* m_session;
        bool isConnected = false;
};

#endif
