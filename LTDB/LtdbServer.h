/*

LtdbServer.h

Header for LtdbServer.cpp
Author: D.Panchal
Email: dpanchal@utexas.edu
Date created: May 23, 2020

*/

#ifndef __LTDB_SERVER_H__
#define __LTDB_SERVER_H__

# define LOG(X) Log(__PRETTY_FUNCTION__, X)

#include <iostream>
#include <memory>

#include <Base/Core/OnlineObject.h>

#include <LTDBSegment/LargDal/LARG_LTDB_HW.h>
#include <LTDBSegment/LargDal/LARG_LTDB_Server.h>

class LtdbServer : public Larg::OnlineObject{
	public:
		LtdbServer(const uint32_t, const std::string&);
		~LtdbServer();

		//////////////////////////// Setter methods ////////////////////////////
		void setServerAddress(const std::string&);
		void setServerId(const std::string&);
		void setNumLtdbs(const uint16_t);
		void CreateLTDBs();
		
		//////////////////////////// Getter methods ////////////////////////////
		const std::string getServerAddress();
		const std::string getServerId();
		const uint16_t getNumLtdbs();

		//////////////////////////// TDAQ FSM implementations ////////////////////////////
		virtual bool Init_();
		virtual bool Load_();
		virtual bool Unload_();
		virtual bool Config_();
		virtual bool PrepareForRun_();
		virtual bool Unconfig_();
		virtual bool Start_();
		virtual bool Stop_();

		bool DefineFromOKS(const uint32_t, const bool, const Larg::dal::LARG_LTDB_HW*&);
		void Log(const std::string&, const std::string& = "");

	private:
		std::vector< std::unique_ptr<Ltdb> > myLtdbs;
		std::string myServerAddress;
		std::string myServerId;
		uint16_t numLtdbs;
};

#endif