/*

LtdbConfig.h

Header for configuration class 
Author: D.Panchal
Email: dpanchal@utexas.edu
Date created: May 11, 2020

*/

// LargOnline include
#include "RodCrate/controller/RunCtrlActions.h"

#include "RodCrate/LargDal/LARG_ConfigurationFile.h"
#include "RodCrate/LargDal/LARG_CTRL_Module.h"
#include "RodCrate/LargDal/LARG_FE_Module.h"
#include "RodCrate/LargDal/LARG_HFEC.h"
#include "RodCrate/LargDal/LARG_IO_Device.h"
#include "RodCrate/LargDal/LARG_IO_Module.h"
#include "RodCrate/LargDal/LARG_DSP_Channel.h"
#include "RodCrate/LargDal/LARG_PU.h"
#include "RodCrate/LargDal/LARG_Module.h"
#include "RodCrate/LargDal/LARG_RODC.h"
#include "RodCrate/LargDal/LARG_FE_Cable.h"
#include <LTDBSegment/LargDal/LARG_LTDB_HW.h>
#include <LTDBSegment/LargDal/LARG_LTDB_Server.h>


#ifndef __LTDB_CONFIG_H__
#define __LTDB_CONFIG_H__


class LtdbConfig : public ConfigDbObject {
  public:
    typedef std::map<std::string, std::pair<int, OnlineObject *> > rodChannelMap_type;

    // Ctor
    LtdbConfig(const std::string& name, RunCtrlActions *rcc);

    // Dtor
    virtual ~LtdbConfig();

    const rodChannelMap_type& GetRodChannelMap(void) const { return myRodChannelMap; }

    bool IsResourceIsDisabled(const daq::core::Component *component);

    bool MapFEBtoRODLinks(void);

    bool SetDetectorIDsToHalfPUs(const std::string& name = "");

  protected:
    typedef std::map<std::string, std::pair<std::string, bool> > febToDSPMap_type;
    typedef std::map<std::string, unsigned char> febToDetectorIDMap_type;

    virtual bool Load_(void);
    virtual bool Unload_(void);

    virtual bool LoadDB(void);
    virtual bool UnloadDB(void);

    virtual bool BuildRODC(const Larg::dal::LARG_RODC *rodc);
    virtual bool BuildHFEC(const Larg::dal::LARG_HFEC *hfec);

    virtual bool CreateModule(unsigned long crateID, const Larg::dal::LARG_Module *module);

    virtual bool CreateModule(unsigned long crateID, const Larg::dal::LARG_FE_Module *module, OnlineObject *hfec,
                              const OnlineObject *spac,
                              const unsigned short master);

    virtual bool CreateModule(unsigned long crateID, const Larg::dal::LARG_FE_Module *module, OnlineObject *hfec,
                              const OnlineObject *ttc);

    bool SetRodDsp(const Larg::dal::LARG_Module *module, OnlineObject *object);
    bool SetLtdbServer(const Larg::dal::LARG_Module *module, OnlineObject *object);

    febToDSPMap_type myFebToDSPMap;
    febToDetectorIDMap_type myFebToDetectorIDMap;

  private:
    bool FindSPAC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *& spac, unsigned short& master);

    bool FindTTC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *& ttc);

    OnlineObject *CreateHfecSPAC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *spac, unsigned short master);

    OnlineObject *CreateHfecTTC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *ttc);

    std::vector<const Larg::dal::LARG_PU *> GetPUs(const Larg::dal::LARG_IO_Module *ioModule);
    std::vector<const Larg::dal::LARG_LTDB_HW *> GetLTDBs(const Larg::dal::LARG_LTDB_Server *opc_server);

    void GetResources(const daq::core::ResourceSet *rset, std::vector<const daq::core::ResourceBase *>& out, Configuration& db);

    bool MapFEBtoRODLink(const std::string& name);
    bool MapFEBtoRODLink(OnlineObject *feb, const Larg::dal::LARG_DSP_Channel *channel);

  private:
    rodChannelMap_type myRodChannelMap;
};

#endif