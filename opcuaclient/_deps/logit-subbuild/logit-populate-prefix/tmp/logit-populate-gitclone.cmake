if("v0.1.1" STREQUAL "")
  message(FATAL_ERROR "Tag for git checkout should not be empty.")
endif()

set(run 0)

if("/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitinfo.txt" IS_NEWER_THAN "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitclone-lastrun.txt")
  set(run 1)
endif()

if(NOT run)
  message(STATUS "Avoiding repeated git clone, stamp file is up to date: '/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitclone-lastrun.txt'")
  return()
endif()

execute_process(
  COMMAND ${CMAKE_COMMAND} -E remove_directory "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to remove directory: '/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt'")
endif()

set(git_options)

# disable cert checking if explicitly told not to do it
set(tls_verify "")
if(NOT "x" STREQUAL "x" AND NOT tls_verify)
  list(APPEND git_options
    -c http.sslVerify=false)
endif()

set(git_clone_options)

set(git_shallow "1")
if(git_shallow)
  list(APPEND git_clone_options --depth 1 --no-single-branch)
endif()

set(git_progress "")
if(git_progress)
  list(APPEND git_clone_options --progress)
endif()

set(git_config "")
foreach(config IN LISTS git_config)
  list(APPEND git_clone_options --config ${config})
endforeach()

# try the clone 3 times in case there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
    COMMAND "/usr/bin/git" ${git_options} clone ${git_clone_options} --origin "origin" "https://github.com/quasar-team/LogIt.git" "LogIt"
    WORKING_DIRECTORY "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries "${number_of_tries} + 1")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS "Had to git clone more than once:
          ${number_of_tries} times.")
endif()
if(error_code)
  message(FATAL_ERROR "Failed to clone repository: 'https://github.com/quasar-team/LogIt.git'")
endif()

execute_process(
  COMMAND "/usr/bin/git" ${git_options} checkout v0.1.1 
  WORKING_DIRECTORY "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to checkout tag: 'v0.1.1'")
endif()

execute_process(
  COMMAND "/usr/bin/git" ${git_options} submodule init 
  WORKING_DIRECTORY "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to init submodules in: '/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt'")
endif()

execute_process(
  COMMAND "/usr/bin/git" ${git_options} submodule update --recursive --init 
  WORKING_DIRECTORY "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: '/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt'")
endif()

# Complete success, update the script-last-run stamp file:
#
execute_process(
  COMMAND ${CMAKE_COMMAND} -E copy
    "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitinfo.txt"
    "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitclone-lastrun.txt"
  WORKING_DIRECTORY "/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/LogIt"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to copy script-last-run stamp file: '/afs/cern.ch/user/d/dpanchal/private/open62541-compat/build/_deps/logit-subbuild/logit-populate-prefix/src/logit-populate-stamp/logit-populate-gitclone-lastrun.txt'")
endif()

