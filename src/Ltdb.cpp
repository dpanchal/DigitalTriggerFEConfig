/*

Ltdb.cpp

Class for the hardware instance of LTDB in TDAQ
Author: D.Panchal
Email: dpanchal@utexas.edu
Date created: April 27, 2020

*/
#include <string>
#include <time.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <iomanip>
// #include <dal/Detector.h>
#include <dal/util.h>

#include "LTDB/Ltdb.h"
#include "uaplatformlayer.h"

using namespace UaClientSdk;

// OPC client connection variables
const unsigned int Ltdb::NUM_CONNECTION_RETRIES = 5;
const unsigned int Ltdb::WAIT_BETWEEN_RETRIES = 10; //milliseconds

/**
* Constructor which creates an instance of Ltdb. In the constructor, an instance of ClientSession is also created. 
* @param dummy integer needed to when called by the configuration plugin
* @param nodeId OPC node identifier for the LTDB board
*/
Ltdb::Ltdb(const uint32_t dummy, const std::string& nodeId) : m_nodeId(nodeId) 
{

    LOG("Creating instance of Ltdb.");
    REGISTER_CLASS(this);
    UaPlatformLayer::init();
    m_client = new ::ClientSession();

}

/**
* Destructor which deletes an instance of Ltdb and also disconnects from the OPC peripheral server. 
*/
Ltdb::~Ltdb()
{

  LOG("Destroying instance of Ltdb.");
  m_client->disconnect();

}

////////////////////// Setter methods //////////////////////
/**
* Set the OPC peripheral server address.
* @param serverAddress OPC peripheral server address
*/
void Ltdb::setServerAddress(const std::string& serverAddress)
{
  m_serverAddress = serverAddress;
}

/**
* Set the serial number of the LTDB board.
* @param serialNumber serial number of the LTDB board (placeholder text for now)
*/
void Ltdb::setSerialNumber(const std::string& serialNumber)
{
  m_serialNumber = serialNumber;
}

/**
* Set the node ID which is the identifier of the LTDB within the peripheral server.
* @param nodeID OPC node identifier
*/
void Ltdb::setNodeId(const std::string& nodeID)
{
  m_nodeId = nodeID;
}

void Ltdb::setMaxAttempts(const uint32_t max_retries)
{
  maxRetries = max_retries;
}
////////////////////////////////////////////////////////////

////////////////////// Getter methods //////////////////////
/**
* Get the LTDB serial number.
* @return serial number if defined, otherwise an empty string
*/
const std::string Ltdb::getSerialNumber()
{
  return m_serialNumber;
}

/**
* Get the OPC peripheral server address.
* @return server address if defined, otherwise an empty string
*/
const std::string Ltdb::getServerAddress()
{
  return m_serverAddress;
}

/**
* Get the node identifier of the LTDB within the peripheral server.
* @return node identifer if defined, otherwise an empty string 
*/
const std::string Ltdb::getNodeId()
{
  return m_nodeId;
}

////////////////////////////////////////////////////////////


////////////////// TDAQ FSM implementations ////////////////
/**
* Initialize FSM which connects to the OPC peripheral server.
* @return true if connected, otherwise false
*/
bool Ltdb::Init_()
{ 
  LOG("Connecting to the OPC peripherial server: " + m_serverAddress);
  
  std::string function = ".init";
  bool connectionSuccessful = false;
  bool isInitialized = false;

  if (m_client->IsConnected()) {
    LOG("Connection already established, calling 'init' method.");
    connectionSuccessful = true;
  }
  else {
    connectionSuccessful = m_client->tryConnect(
                                                  m_serverAddress.c_str(),
                                                  NUM_CONNECTION_RETRIES,
                                                  WAIT_BETWEEN_RETRIES
                                                );    
  }

  if (connectionSuccessful) {
    isInitialized = m_client->call(m_nodeId.c_str(), function.c_str(), maxRetries);
  }
  else {
    LOG("Cannot connect to the OPC peripheral server: " + m_serverAddress);
  }
  
  return isInitialized;

}

/**
* Load FSM which calls the "configure" function to the LTDB node in the peripheral server.
* @return true if "configure" function succeeded, otherwise false
*/
bool Ltdb::Load_()
{

  LOG("Calling configure function.");
  std::string function = ".configure";

  bool isLTDBConfigured = m_client->call(m_nodeId.c_str(), function.c_str(), maxRetries);

  return isLTDBConfigured;

}

/**
* Unload FSM which calls the "shutdown" function to the LTDB node in the peripheral server.
* @return true if "shutdown" function succeeded, otherwise false
*/
bool Ltdb::Unload_()
{ 
  
  LOG("Calling unload function.");
  // std::string function = ".shutdown";
  // bool isLTDBUnconfigured = m_client->call(m_nodeId.c_str(), function.c_str());
  // return isLTDBUnconfigured;
  return true;

}

/**
* Config FSM which calls the "configure" function to the LTDB node in the peripheral server.
* Once the configuration is done, sleep for 5 seconds before function return.
* @return true if configuration succeeded, otherwise false
*/
bool Ltdb::Config_()
{

  // LOG("Calling configure function.");
  LOG("Connecting to the node: " + m_nodeId);
  // Set a manual sleep for 5 seconds
  std::this_thread::sleep_for(std::chrono::seconds(5));
  return true;
  
}

/**
* Unconfig FSM which simply returns true (no need to unconfigure).
* @return true
*/
bool Ltdb::Unconfig_()
{

  LOG("Calling unconfigure function.");
  // std::string function = ".unConfigure";
  // bool isConnectionShut = m_client->call(m_nodeId.c_str(), function.c_str());
  // return isConnectionShut;
  return true;

}

/**
* Start FSM which simply returns true.
* @return true 
*/
bool Ltdb::Start_()
{
  LOG("Calling start function.");
  return true;
}

/**
* Stop FSM which simply returns true.
* @return true 
*/
bool Ltdb::Stop_()
{
  LOG("Calling stop function.");
  return true;
}

/**
* PrepareForRun FSM which simply returns true.
* @return true 
*/
bool Ltdb::PrepareForRun_()
{
  LOG("Calling prepare for run function.");
  return true;
}
////////////////////////////////////////////////////////////

/**
* Method to set the class variables from OKS file.
* @param slotID index of the LTDB board within the LTDB rack
* @param enabled whether the LTDB is enabled or not
* @param params parameters for the LTDB read from the OKS file
* @return true if the class variables were successfully initialized, otherwise false
*/
bool Ltdb::DefineFromOKS(const Larg::dal::LARG_LTDB_HW*& params) 
{
    LOG("Setting up LTDB OKS parameters");

    if (params == NULL) {
        const std::string& msg = "Received NULL value for parameter 'params'.";
        LOG(msg);
        return false;
    }

  const std::string serialNumber = params->get_SerialNumber();
  if (serialNumber.empty()) {
    const std::string& msg = "LTDB serial number is undefined";
    LOG(msg);
    return false;
  }

  const std::string nodeID = params->get_nodeId();
  if (nodeID.empty()) {
    const std::string& msg = "LTDB node ID is undefined";
    LOG(msg);
    return false;
  }
  
  const uint32_t max_retries = params->get_numRetries();
  if (max_retries < 0) {
    const std::string& msg = "Maximum number of retries is less than zero.";
    LOG(msg);
    return false;
  }

  setSerialNumber(serialNumber);
  setNodeId(nodeID);
  setMaxAttempts(max_retries);

  return true;
}

/**
* Internal logging facility called using LOG() macro
*/
void Ltdb::Log(const std::string& pretty, const std::string& details)
{
    // Try to get current time at TDAQ::ERS default format
  std::string time = "UNKNOWN TIME";
  struct timeval tp;
  if (!gettimeofday(&tp, NULL)) {
    // YYYY-Mon-dd hh:mm:ss
    struct tm* timeinfo = localtime(&tp.tv_sec);
    char buffer[32];
    size_t s = strftime(buffer, sizeof (buffer), "%G-%h-%d %T", timeinfo); // Format: 2020-Mar-06 07:21:39
    if (s < sizeof (buffer)) {
      // Milliseconds on 3 digits
      std::stringstream stream;
      stream << std::setfill('0') << std::setw(3) << std::dec << std::to_string(tp.tv_usec / 1000);
      time = std::string(buffer) + "," + stream.str();
    }
  }

  std::cout << time + " - [LTDB-BOARD_" + getSerialNumber() + "] - " + pretty + (details.length() > 0 ? " : " + details : "") << std::endl;

}

// set auto-registration, used by Dynamic Class Loader
// NOTA: the macro use C function (i.e. use non-POD type).
// Replace 'const string&' by 'const char *'
ONLINE_DCL_RECORD_CLASS_2P(Ltdb, const uint32_t, const char *)
