/*
*  ClientSession.cpp
*
* Created on: April 20, 2020
* Author: D. Panchal
*
*
*/

#include <iostream>
// #include <string>
#include <unistd.h>
#include "LTDB/ClientSession.h"

using namespace UaClientSdk;

/**
* Constructor which creates a new UaSession.
*/
ClientSession::ClientSession() 
{   
    m_session = new UaSession();
}

/**
* Destructor which deletes an existing UaSession.
*/
ClientSession::~ClientSession()
{
    if (m_session) {
        delete m_session;
        m_session = NULL;
    }
}

/**
* Connect to the OPC server.
* @param serverAddress address of the OPC server
* @return true if connected, otherwise false
*/
bool ClientSession::connect(const UaString& serverAddress) {
  
    UaClientSdk::SessionSecurityInfo security;

    UaClientSdk::SessionConnectInfo sessionConnectInfo;
    sessionConnectInfo.sApplicationName = "Client_Cpp_SDK@myComputer";
    sessionConnectInfo.sApplicationUri  = "Client_Cpp_SDK@myComputer";
    sessionConnectInfo.sProductUri      = "Client_Cpp_SDK";
    sessionConnectInfo.internalServiceCallTimeout = 60000;
    /*********************************************************************
     Connect to OPC UA Server
    **********************************************************************/
    UaStatus status = m_session->connect(
                                        serverAddress, // URL of the Endpoint - from discovery or config
                                        sessionConnectInfo,  // General settings for connection
                                        security, // Security settings
                                        nullptr );        // Callback interface

    if (status.isBad()) {
        std::cout << __PRETTY_FUNCTION__ << "Error in connecting to the peripheral server." << std::endl;
        return false;
    }
    isConnected = true;

    return true;
}

bool ClientSession::tryConnect(const UaString& serverAddress, 
                               unsigned int numRetries,
                               unsigned int waitBetweenRetry)
{
    if (numRetries<1) {
        throw std::runtime_error("Number of retries should be greater than one.");
    }

    unsigned int numAttempts = 1;
    bool connectionSuccessful = false;
    do {
        connectionSuccessful = connect(serverAddress);

        if (connectionSuccessful) {
            std::cout << __PRETTY_FUNCTION__ << " Connected to " << serverAddress.toUtf8() << " after " << numAttempts << " attempts" << std::endl;
            return connectionSuccessful;
        }

        else {
            if (numAttempts < numRetries) {
                std::cout << __PRETTY_FUNCTION__ << " Retrying connection, attempt " << numAttempts << " after delay of " << waitBetweenRetry << " ms." << std::endl;
                usleep(waitBetweenRetry*1000);
            }
        }
        numAttempts++;
    }
    while (numAttempts < numRetries);
    std::cout << __PRETTY_FUNCTION__ << " Falied to connect to " << serverAddress.toUtf8() << " after " << numRetries << "tries." << std::endl;
    
    return connectionSuccessful;
}

bool ClientSession::IsConnected()
{
    return isConnected;
}

/**
* Disconnect from the OPC server.
* @return true if disconnected, otherwise false
*/
bool ClientSession::disconnect() {

    UaStatus status;

    ServiceSettings serviceSettings;
    if (m_session) {
        status = m_session->disconnect(
                                        serviceSettings,
                                        OpcUa_True );

        if (status.isBad()) {
            throw UaoClientForOpcUaSca::Exceptions::BadStatusCode ("Error in disconnect ", status.statusCode());
            return false;
        }
    }

    isConnected = false;
    return true;
}

/**
* Call a method to node on the OPC server.
* @param nodeToConfigure node to address on the peripheral server
* @param function name of the method to execute on the node
* @return true if method executed successfully, otherwise false
*/
bool ClientSession::call(const char* nodeToConfigure, 
						 const char* function,
						 unsigned int maxRetries) 
{

    UaNodeId nodeId (UaString (nodeToConfigure), 2);  // DP: don't know why this hardcoded 2?

    ServiceSettings serviceSettings;
    CallIn callRequest;
    CallOut co;

    callRequest.objectId = nodeId;
    callRequest.methodId = UaNodeId (UaString (nodeId.identifierString()) + UaString (function), 2);

    unsigned int numAttempts = 0;
    UaStatus status;
    do {
            numAttempts++;
	    UaVariant v;
	    status = m_session->call(serviceSettings, callRequest, co);
	    if (status.isBad()) {
	    	if (numAttempts < maxRetries) {
		    	std::cout << __PRETTY_FUNCTION__ << " Retrying function " << std::string(function) << ", attempt " << numAttempts << std::endl;
	    	}
	    	else {
		  std::cout << __PRETTY_FUNCTION__ << " Unable to execute function " << std::string(function) << " after " << maxRetries << " attempts." << std::endl;
		  //				throw UaoClientForOpcUaSca::Exceptions::BadStatusCode ("In OPC-UA call", status.statusCode());    		
	    	}
	    }
	    else {
	    	return true;
	    }
	    //	    numAttempts++;
    }
    while (numAttempts < maxRetries);
    throw UaoClientForOpcUaSca::Exceptions::BadStatusCode ("In OPC-UA call", status.statusCode());
    return false;
}

bool ClientSession::write(const char* nodeToWrite,
                          const char* functionToWrite,
                          const bool data)
{
    ServiceSettings ss;
    UaWriteValues nodesToWrite;
    UaDataValues dataValues;
    UaDiagnosticInfos diagnosticInfos;
    UaStatusCodeArray results;

    UaNodeId ltdb_node (UaString (nodeToWrite), 2);

    UaNodeId functionId (UaString (ltdb_node.identifierString()) + UaString(functionToWrite), ltdb_node.namespaceIndex());

    nodesToWrite.create(1);
    functionId.copyTo( &nodesToWrite[0].NodeId );
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;

    UaVariant v ( (OpcUa_Boolean)(data) );

    dataValues.create(1);
    v.copyTo( &nodesToWrite[0].Value.Value );

    UaStatus status = m_session->write(
                                        ss,
                                        nodesToWrite,
                                        results,
                                        diagnosticInfos
                                        );

    if (status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("OPC-UA write failed ", status.statusCode());
    }

    if (results[0] != OpcUa_Good) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("OPC-UA write failed ", results[0]);
    }

    return true;

}


UaDataValues ClientSession::readData(const char* nodeToRead,
                                     const char* function,
                                     UaStatus *out_status)
{
    ServiceSettings ss;
    UaReadValueIds nodesToRead;
    UaDataValues dataValues;
    UaDiagnosticInfos diagnosticInfos;

    UaNodeId ltdb_node (UaString (nodeToRead), 2); //Maybe not the correct namespace index

    UaNodeId functionId (UaString (ltdb_node.identifierString()) + UaString(function), ltdb_node.namespaceIndex());

    nodesToRead.create(1);
    functionId.copyTo( &nodesToRead[0].NodeId );
    nodesToRead[0].AttributeId = OpcUa_Attributes_Value;

    dataValues.create(1);

    UaStatus status = m_session->read(
                                        ss,
                                        0,
                                        OpcUa_TimestampsToReturn_Both,
                                        nodesToRead,
                                        dataValues,
                                        diagnosticInfos
                                        );

    if (status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("In OPC-UA readData ", status.statusCode());
    }
    if (out_status) {
        *out_status = dataValues[0].StatusCode;
    }

    return dataValues;
}

std::string ClientSession::readName(const char* nodeToRead)
{
    std::string function_name = ".ltdb_name";
    UaStatus out_status;

    UaDataValues readValue = readData(nodeToRead, function_name.c_str(), &out_status);

    UaString out;

    if (out_status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("Read status is not good", out_status.statusCode());

    }

    out = UaVariant(readValue[0].Value).toString();

    return out.toUtf8();
}

uint32_t ClientSession::readInt32(const char* nodeToRead,
                                  const char* function)
{
    UaStatus out_status;

    UaDataValues readValue = readData(nodeToRead, function, &out_status);

    OpcUa_Int32 out;

    if (out_status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("Read status is not good", out_status.statusCode());
    }

    UaStatus conversionStatus = (UaVariant(readValue[0].Value)).toInt32 (out);
    if (! conversionStatus.isGood())
    {
        throw std::runtime_error(std::string("OPC-UA read: read succeeded but conversion to native type failed (was it NULL value?): ") + UaStatus(readValue[0].StatusCode).toString().toUtf8() );
    }

    return (uint32_t) out;
}

bool ClientSession::readBoolean(const char* nodeToRead,
                                const char* function)
{
    UaStatus out_status;

    UaDataValues readValue = readData(nodeToRead, function, &out_status);

    OpcUa_Boolean out;

    if (out_status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("Read status is not good", out_status.statusCode());
    }

    UaStatus conversionStatus = (UaVariant(readValue[0].Value)).toBool (out);
    if (! conversionStatus.isGood())
    {
        throw std::runtime_error(std::string("OPC-UA read: read succeeded but conversion to native type failed (was it NULL value?): ") + UaStatus(readValue[0].StatusCode).toString().toUtf8() );
    }

    return (bool) out;
}

uint8_t ClientSession::readByte(const char* nodeToRead,
                                const char* function)
{
    UaStatus out_status;

    UaDataValues readValue = readData(nodeToRead, function, &out_status);

    OpcUa_Byte out;

    if (out_status.isBad()) {
        throw UaoClientForOpcUaSca::Exceptions::BadStatusCode("Read status is not good", out_status.statusCode());
    }

    UaStatus conversionStatus = (UaVariant(readValue[0].Value)).toByte (out);
    if (! conversionStatus.isGood())
    {
        throw std::runtime_error(std::string("OPC-UA read: read succeeded but conversion to native type failed (was it NULL value?): ") + UaStatus(readValue[0].StatusCode).toString().toUtf8() );
    }

    return (uint8_t) out;
}