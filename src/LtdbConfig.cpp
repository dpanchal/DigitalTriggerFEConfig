/*

LtdbConfig.cpp

Barebones configuration class, inspired by LARGConfig
Author: D.Panchal
Email: dpanchal@utexas.edu
Date created: May 11, 2020

*/

#include <algorithm>

// LargOnline include
#include <Rod/Rod.h>
#include <Spac/SpacAbst.h>
#include <Base/Core/Utils.h>
#include <RodCrate/monitoring/ModuleMonitoring.h>
#include <RodCrate/controller/RccExceptions.h>

#include "LTDB/Ltdb.h"
#include "LTDB/LtdbServer.h"
#include "LTDB/LtdbConfig.h"

// DAQ include
#include <dal/util.h>
#include <dal/Detector.h>
#include <ers/ers.h>

using namespace Larg::util;

LtdbConfig::LtdbConfig(const std::string& name, RunCtrlActions * rcc) : 
	ConfigDbObject(name, rcc)
{
  ERS_DEBUG(0, "Creating instance of LTDB configuration plugin with name " << name);
	REGISTER_CLASS(this);
}

LtdbConfig::~LtdbConfig()
{

  myFebToDSPMap.clear();
  myFebToDetectorIDMap.clear();
  myRodChannelMap.clear();

}

bool LtdbConfig::BuildRODC(const Larg::dal::LARG_RODC *rodc)
{
  bool rtn = true;

  try {
    if (!rodc || !rodc->get_State()) return true;

    std::cout << __PRETTY_FUNCTION__ << ": Found ROD crate " << rodc->UID() << std::endl;

    const std::vector<const daq::core::Module *> modules = rodc->get_Modules();

    std::cout << "\tHas " << modules.size() << " module(s)" << std::endl;

    for (daq::core::ModuleIterator it = modules.begin(); it != modules.end(); ++it) {
      rtn &= CreateModule(rodc->get_LogicalId(),
        Rcc()->configuration()->cast<Larg::dal::LARG_Module, daq::core::Module> (*it));
    }

    const std::vector<const Larg::dal::LARG_HFEC *> hfecs = rodc->get_Controls();

    std::cout << "\t\tControls " << hfecs.size() << " HFEC crate(s)" << std::endl;

    // Read all FEB/ROD cables in partition

    if (hfecs.size() > 0) {
      std::vector<const Larg::dal::LARG_FE_Cable *> cables;

      Rcc()->configuration()->get(cables);

      for (size_t ii = 0; ii < cables.size(); ++ii) {
        myFebToDSPMap.insert(febToDSPMap_type::value_type(cables[ii]->get_Source()->UID(),
            std::make_pair(cables[ii]->get_Destination()->UID(),
              cables[ii]->get_State())));
      }
    }

    for (Larg::dal::LARG_HFECIterator it = hfecs.begin(); it != hfecs.end(); ++it)
      rtn &= BuildHFEC(*it);

    MapFEBtoRODLinks();
    SetDetectorIDsToHalfPUs();
  }
  catch (ers::Issue& ex) {
    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot build RODC", ex);
    ers::error(issue);

    return false;
  }

  return rtn;
}

bool LtdbConfig::BuildHFEC(const Larg::dal::LARG_HFEC *hfec)
{
  if (!hfec) return true;

  {
    const std::vector<const daq::core::Module *> modules = hfec->get_Modules();

    for (daq::core::ModuleIterator itm = modules.begin(); itm != modules.end(); ++itm) {
      febToDSPMap_type::iterator it = myFebToDSPMap.find((*itm)->UID());

      if (it != myFebToDSPMap.end())
        myFebToDetectorIDMap[it->first] = hfec->get_BelongsTo()->get_LogicalId();
    }
  }

  unsigned short master;
  OnlineObject *spac = NULL;
  OnlineObject *ttc  = NULL;

  FindSPAC(hfec, spac, master);
  FindTTC(hfec, ttc);

  if (!hfec->get_State()) return true;

  ERS_DEBUG(0, ": Found HFEC crate " << hfec->UID());

  OnlineObject *hfecSPAC = CreateHfecSPAC(hfec, spac, master);
  OnlineObject *hfecTTC  = CreateHfecTTC(hfec, ttc);

  bool rtn                                             = true;
  const std::vector<const daq::core::Module *> modules = hfec->get_Modules();

  std::cout << "\tHas " << modules.size() << " module(s)" << std::endl;

  for (daq::core::ModuleIterator it = modules.begin(); it != modules.end(); ++it) {
    rtn &= CreateModule(hfec->get_LogicalId(),
      Rcc()->configuration()->cast<Larg::dal::LARG_FE_Module, daq::core::Module> (*it),
      hfecSPAC, spac, master);

    rtn &= CreateModule(hfec->get_LogicalId(),
      Rcc()->configuration()->cast<Larg::dal::LARG_FE_Module, daq::core::Module> (*it),
      hfecTTC, ttc);
  }

  return rtn;
}

OnlineObject *LtdbConfig::CreateHfecSPAC(const Larg::dal::LARG_HFEC *hfec,
                                         OnlineObject *              spac,
                                         unsigned short              master)
{
  OnlineObject *object = NULL;

  if (spac) {
    Create_type *creator = OnlineFactoryCreate["HfecSPAC"];
    if (creator) {
      object = creator(spac, master, hfec->UID().c_str());

      if (object) {
        RunCtrlActions::Crate *crate;

        if (Rcc()->GetCrate(crate)) object->SetCrateID(crate->GetID());

        object->AddDetectorID(hfec->get_BelongsTo()->get_LogicalId());
        object->SetID(hfec->get_LogicalId());

        if (hfec->get_LogicalId() > UCHAR_MAX) {
          Larg::rcc::BadID issue(ERS_HERE,
                                 hfec->get_LogicalId(),
                                 hfec->UID() + "@HfecSPAC");
          ers::warning(issue);
        }

        Rcc()->AddObject("HFECSPAC", object);
        Rcc()->AddControllable(object, OnlineObject::GetDefaultOrder());
      }
    }
  }

  return object;
}

OnlineObject *LtdbConfig::CreateHfecTTC(const Larg::dal::LARG_HFEC *hfec,
                                        OnlineObject *              ttc)
{
  OnlineObject *object = NULL;

  if (ttc) {
    Create_type *creator = OnlineFactoryCreate["HfecTTC"];
    if (creator) {
      object = creator(ttc, hfec->UID().c_str());

      if (object) {
        RunCtrlActions::Crate *crate;

        if (Rcc()->GetCrate(crate)) object->SetCrateID(crate->GetID());

        object->AddDetectorID(hfec->get_BelongsTo()->get_LogicalId());
        object->SetID(hfec->get_LogicalId());

        if (hfec->get_LogicalId() > UCHAR_MAX) {
          Larg::rcc::BadID issue(ERS_HERE,
                                 hfec->get_LogicalId(),
                                 hfec->UID() + "@HfecTTC");
          ers::warning(issue);
        }

        Rcc()->AddObject("HFECTTC", object);
        Rcc()->AddControllable(object, OnlineObject::GetDefaultOrder());
      }
    }
  }

  return object;
}

bool LtdbConfig::CreateModule(unsigned long                 crateID,
                              const Larg::dal::LARG_Module *module)
{
  std::cout << __PRETTY_FUNCTION__ << ": " << module->UID() << "@"
            << module->get_Model() << " state is " << module->get_State()
            << std::endl;

  if (!module->get_State()) return true;

  try {
    if ((module->class_name() == Larg::dal::LARG_IO_Module::s_class_name)) {
      const daq::core::ResourceSetAND *resourceSet = Rcc()->configuration()->get<daq::core::ResourceSetAND> (module->UID());

      if (IsResourceIsDisabled(resourceSet)) return true;
    }
  }
  catch (ers::Issue& ex) {
    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot create module", ex);
    ers::error(issue);

    return false;
  }

  const Larg::dal::LARG_ConfigurationFile *configFile = module->get_HasConfigurationFile();
  std::string filename;

  if (configFile) filename = configFile->get_Filename();

  Create_type *creator = OnlineFactoryCreate[module->get_Model()];

  if (creator) {
    std::cout << __PRETTY_FUNCTION__ << ": call creator () of class "
              << module->get_Model() << std::endl;

    OnlineObject *o = creator(module->get_PhysAddress(), module->UID().c_str());

    if (o) {
      o->SetDbParams(filename);
      o->SetDetectorIDs(Rcc()->GetDetectorLogicalIDs());
      o->SetCrateID(crateID);
      o->SetID(module->get_LogicalId());
      o->SerialNumber.Set(module->get_SerialNumber());
      o->SetExtraInfo(module->get_ExtraInformation());

      std::cout << __PRETTY_FUNCTION__ << ": [OKS] " << module->UID() << "@" << module->class_name() << std::endl;
      std::cout << __PRETTY_FUNCTION__ << ": [OnlineObject] " << o->GetName() << "@" << o->GetClassName() << std::endl;

      if (module->class_name() == Larg::dal::LARG_IO_Module::s_class_name)
        SetRodDsp(module, o);
      else if (module->class_name() == Larg::dal::LARG_LTDB_Server::s_class_name)
        SetLtdbServer(module, o);

      if (!o->Probe()) {
        Larg::rcc::BadModuleSetting issue(ERS_HERE, o->GetName());
        ers::warning(issue);

        OnlineFactoryDestroy[module->get_Model()] (o);

        ERS_DEBUG(0, "Call destructor () of class " << module->get_Model());

        return true;
      }

      Rcc()->InsertEntry(module->UID(), Rcc()->GetCrateName());
      Rcc()->AddObject(module->get_Type(), o);
      Rcc()->AddControllable(o, module->get_OrderedActions());

      if (module->get_Monitored()) {
        if (o->IsKindOf("Rod") || o->IsInheritFrom("Rod")) {
          OnlineObject::objects_type objects = OnlineObject::GetFamillyObjects("ExtIoCtrlFactory<RodMonitoring>");

          if (objects.empty()) {
            std::cout << __PRETTY_FUNCTION__ << ": module " << o->GetClassName()
                      << " " << o->GetName() << " is monitored" << std::endl;

            Rcc()->CreateThread(std::string("ModuleMonitoring<") + o->GetName() + ">",
              new ModuleMonitoring(Rcc(), o));
          } else {
            std::cout << __PRETTY_FUNCTION__ << ": module " << o->GetClassName()
                      << " " << o->GetName()
                      << " is globally monitored by External I/O plugin \""
                      << objects[0]->GetName() << "@"
                      << objects[0]->GetClassName() << "\"" << std::endl;
          }
        } else {
          std::cout << __PRETTY_FUNCTION__ << ": module " << o->GetClassName()
                    << " " << o->GetName() << " is monitored" << std::endl;

          Rcc()->CreateThread(std::string("ModuleMonitoring<") + o->GetName() + ">",
            new ModuleMonitoring(Rcc(), o));
        }
      }
    } else {
      Larg::rcc::CantCreateObject issue(ERS_HERE,
                                        module->UID() + "@" + module->get_Model());
      ers::warning(issue);
    }
  } else {
    Larg::rcc::ClassCreatorNotFound issue(ERS_HERE, module->get_Model());
    ers::warning(issue);
  }

  return true;
}

bool LtdbConfig::CreateModule(unsigned long                    crateID,
                              const Larg::dal::LARG_FE_Module *module,
                              OnlineObject *                   hfec,
                              const OnlineObject *             spac,
                              const unsigned short             master)
{
  if ((!module->get_State()) || !hfec) return true;

  const Larg::dal::LARG_ConfigurationFile *configFile = module->get_HasConfigurationFile();
  std::string filename;

  if (configFile) filename = configFile->get_Filename();

  std::string model = module->get_Model() + "SPAC";

  Create_type *creator = OnlineFactoryCreate[model];

  if (creator) {
    OnlineObject *o;

    if (module->class_name() == Larg::dal::LARG_FE_Module::s_class_name) {
      o = creator(spac, master, module->get_TtcRxAddress(),
        module->get_SpacAddress(), module->UID().c_str());
    } else {
      const Larg::dal::LARG_CTRL_Module *ctrlModule =
        Rcc()->configuration()->cast<Larg::dal::LARG_CTRL_Module, Larg::dal::LARG_FE_Module> (module);

      o = creator(spac, master, ctrlModule->get_TtcRxAddress(), ctrlModule->get_TtcRxAddress2(),
        ctrlModule->get_SpacAddress(), ctrlModule->get_SpacAddress2(), ctrlModule->UID().c_str());
    }

    if (o) {
      o->SetDbParams(filename);
      o->AddDetectorID(*hfec->GetDetectorIDs().begin());
      o->SetCrateID(crateID);

      febToDSPMap_type::iterator it = myFebToDSPMap.find(module->UID());
      unsigned int id               = module->get_HWIdentifier();

      if (it != myFebToDSPMap.end()) {
        ERS_DEBUG(0, "Cable state FEB(" << it->first <<
          ")/DSP(" << it->second.first <<
          ") = " << it->second.second);

        if (!it->second.second)   // Check if FEB/DSP cable state is disabled
          id = 0U;
      }

      o->SetID(id);
      o->SerialNumber.Set(module->get_SerialNumber());
      o->SetExtraInfo(module->get_ExtraInformation());

      if (!o->Probe()) {
        Larg::rcc::BadModuleSetting issue(ERS_HERE, o->GetName());
        ers::warning(issue);

        OnlineFactoryDestroy[model] (o);

        ERS_DEBUG(0, "Call destructor () of class " << model);

        return true;
      }

      ERS_DEBUG(1, "Add into the crate '" << hfec->GetName()
                                          << "' the module '" << o->GetName() << "@"
                                          << o->GetClassName() << "' ["
                                          << module->get_Type() << "]");

      if (!hfec->Add(module->get_Type(), o)) {
        OnlineFactoryDestroy[model] (o);
        return true;
      }

      Rcc()->InsertEntry(module->UID(), hfec->GetName());
      Rcc()->AddObject(module->get_Type(), o);

      if (module->get_Monitored()) {
        std::cout << __PRETTY_FUNCTION__ << ": module " << o->GetClassName()
                  << " " << o->GetName() << " is monitored" << std::endl;

        Rcc()->CreateThread(std::string("ModuleMonitoring<") + o->GetName() + ">",
          new ModuleMonitoring(Rcc(), o));
      }
    } else {
      Larg::rcc::ClassCreatorNotFound issue(ERS_HERE, model);
      ers::warning(issue);
    }
  }

  return true;
}

bool LtdbConfig::CreateModule(unsigned long                    crateID,
                              const Larg::dal::LARG_FE_Module *module,
                              OnlineObject *                   hfec,
                              const OnlineObject *             ttc)
{
  if ((!module->get_State()) || !hfec) return true;

  const Larg::dal::LARG_ConfigurationFile *configFile = module->get_HasTtcConfigurationFile();
  std::string filename;

  if (configFile) filename = configFile->get_Filename();

  std::string model = module->get_Model() + "TTC";

  Create_type *creator = OnlineFactoryCreate[model];

  if (creator) {
    std::cout << __PRETTY_FUNCTION__ << ": call creator () of class "
              << module->get_Model() << std::endl;

    OnlineObject *o = creator(ttc, module->get_TtcRxAddress(), module->UID().c_str());

    if (o) {
      o->SetDbParams(filename);
      o->AddDetectorID(*hfec->GetDetectorIDs().begin());
      o->SetCrateID(crateID);
      o->SetID(module->get_HWIdentifier());
      o->SerialNumber.Set(module->get_SerialNumber());
      o->SetExtraInfo(module->get_ExtraInformation());

      if (!o->Probe()) {
        Larg::rcc::BadModuleSetting issue(ERS_HERE, o->GetName());
        ers::warning(issue);

        OnlineFactoryDestroy[model] (o);

        ERS_DEBUG(0, "Call destructor () of class " << model);

        return true;
      }

      if (!hfec->Add(module->get_Type(), o)) {
        OnlineFactoryDestroy[model] (o);
        return true;
      }

      Rcc()->InsertEntry(module->UID(), hfec->GetName());
      Rcc()->AddObject(module->get_Type(), o);

      std::string type(module->get_Type());
      std::transform(type.begin(), type.end(), type.begin(), ToLower());

      if (type != "feb") {
        if (module->get_Monitored()) {
          std::cout << __PRETTY_FUNCTION__ << ": module " << o->GetClassName()
                    << " " << o->GetName() << " is monitored" << std::endl;

          Rcc()->CreateThread(std::string("ModuleMonitoring<") + o->GetName() + ">",
            new ModuleMonitoring(Rcc(), o));
        }
      }
    } else {
      Larg::rcc::ClassCreatorNotFound issue(ERS_HERE, model);
      ers::warning(issue);
    }
  }

  return true;
}

bool LtdbConfig::FindSPAC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *& spac,
                          unsigned short& master)
{
  const Larg::dal::LARG_IO_Device *ioDevice = hfec->get_DrivenBySPAC();

  if (ioDevice) {
    master = ioDevice->get_DeviceId();
    spac   = NULL;

    OnlineObject::objects_type objects      = OnlineObject::GetFamillyObjects("SpacAbst");
    OnlineObject::objects_type::iterator it = objects.begin();

    while ((it != objects.end()) && !spac) {
      if ((*it)->GetName() == ioDevice->get_PartOf()->UID()) spac = (*it);

      ++it;
    }

    if (spac) {
      // Set detector ID for SPAC master
      SpacMasterAbst *spacMaster = dynamic_cast<SpacAbst *> (spac)->GetMaster(master);
      spacMaster->AddDetectorID(hfec->get_BelongsTo()->get_LogicalId());
    }
  }

  return spac != NULL;
}

bool LtdbConfig::FindTTC(const Larg::dal::LARG_HFEC *hfec, OnlineObject *& ttc)
{
  const Larg::dal::LARG_IO_Device *ioDevice = hfec->get_DrivenByTTC();

  if (ioDevice) {
    ttc = NULL;

    OnlineObject::objects_type objects      = OnlineObject::GetFamillyObjects("TTCvi");
    OnlineObject::objects_type::iterator it = objects.begin();

    while ((it != objects.end()) && !ttc) {
      if ((*it)->GetName() == ioDevice->get_PartOf()->UID()) ttc = (*it);

      ++it;
    }
  }

  return ttc != NULL;
}

std::vector<const Larg::dal::LARG_PU *> LtdbConfig::GetPUs(const Larg::dal::LARG_IO_Module *ioModule)
{
  std::vector<const Larg::dal::LARG_PU *> PUs;
  std::vector<const daq::core::ResourceBase *> out;

  if (ioModule) {
    GetResources(Rcc()->configuration()->cast<daq::core::ResourceSet> (ioModule),
      out,
      *Rcc()->configuration());

    std::vector<const daq::core::ResourceBase *>::const_iterator it;

    for (it = out.begin(); it != out.end(); ++it) {
      if ((*it)->class_name() == Larg::dal::LARG_PU::s_class_name)
        PUs.push_back(Rcc()->configuration()->cast<Larg::dal::LARG_PU> ((*it)));
    }
  }

  return PUs;
}

std::vector<const Larg::dal::LARG_LTDB_HW*> LtdbConfig::GetLTDBs(const Larg::dal::LARG_LTDB_Server *opc_server)
{
  std::vector<const Larg::dal::LARG_LTDB_HW *> LTDBs;
  std::vector<const daq::core::ResourceBase *> out;

  if (opc_server) {
    GetResources(Rcc()->configuration()->cast<daq::core::ResourceSet> (opc_server),
      out,
      *Rcc()->configuration());

    std::vector<const daq::core::ResourceBase *>::const_iterator it;

    // Find the LTDB hardware objects and add to the output vector if the resource is enabled
    for (it = out.begin(); it != out.end(); ++it) {
      if ((*it)->class_name() == Larg::dal::LARG_LTDB_HW::s_class_name && !IsResourceIsDisabled(*it))
        LTDBs.push_back(Rcc()->configuration()->cast<Larg::dal::LARG_LTDB_HW> ((*it)));
    }
  }

  return LTDBs;

}

void LtdbConfig::GetResources(const daq::core::ResourceSet *                rset,
                              std::vector<const daq::core::ResourceBase *>& out,
                              Configuration&                                db)
{
  std::vector<const daq::core::ResourceBase *>::const_iterator it;

  for (it = rset->get_Contains().begin(); it != rset->get_Contains().end(); ++it) {
    if (const daq::core::ResourceSet * s = db.cast<daq::core::ResourceSet> (*it)) {
      out.push_back(*it);
      GetResources(s, out, db);
    } else {
      out.push_back(*it);
    }
  }
}

bool LtdbConfig::IsResourceIsDisabled(const daq::core::Component *component)
{
  const daq::core::Partition *partition = daq::core::get_partition(*Rcc()->configuration(),
    Rcc()->partition().name());

  if (!partition)
    throw Larg::rcc::PartitionObjectNotFound(ERS_HERE, Rcc()->partition().name());

  return component->disabled(*partition);
}

bool LtdbConfig::SetLtdbServer(const Larg::dal::LARG_Module *module, 
				                     OnlineObject * 				       object)
{
	const Larg::dal::LARG_LTDB_Server *opc_server = Rcc()->configuration()->cast<Larg::dal::LARG_LTDB_Server, Larg::dal::LARG_Module> (module);

  std::vector<const Larg::dal::LARG_LTDB_HW *> LTDBs = GetLTDBs(opc_server);

	LtdbServer *ltdb_server = dynamic_cast<LtdbServer *> (object);
	if (ltdb_server==NULL) {
	    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot retrieve Ltdb server");
	    ers::error(issue);
	    return false;
	}

  const string& serverAddress = opc_server->get_serverAddress();
  try {
    ltdb_server->setServerAddress(serverAddress);
  }
  catch (ers::Issue& ex) {
    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot configure LTDB Server'" + serverAddress + "'", ex);
    ers::error(issue);
    return false;
  }

  const string& serverID = opc_server->get_serverID();
  try {
    ltdb_server->setServerId(serverID);
  }
  catch (ers::Issue& ex) {
    Larg::rcc::BadConfiguration issue(ERS_HERE, "Invalid server ID '" + serverID + "'", ex);
    ers::error(issue);
    return false;
  }
  const uint16_t num_ltdbs = LTDBs.size();
  try {
    ltdb_server->setNumLtdbs(num_ltdbs);
    ltdb_server->CreateLTDBs();
  }

  catch (ers::Issue& ex) {
    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot configure LTDB Server with '" + to_string(num_ltdbs) + "' LTDBs", ex);
    ers::error(issue);
    return false;
  }

	try {
    for (size_t i = 0; i < LTDBs.size(); ++i) {

      const string& ltdb_board = " :LTDB[" + to_string(i) + "] - ";
      std::cout << __PRETTY_FUNCTION__ << ltdb_board << "//////////////////////////////////////////////////////////////////////////////" << std::endl; 

      const bool enabled = !IsResourceIsDisabled(LTDBs[i]); // DP: Need to implement enabled function
      // const bool enabled = true;
      std::cout << __PRETTY_FUNCTION__ << ltdb_board << "enabled = " << enabled << "." << std::endl;

      const uint32_t index = i;
      std::cout << __PRETTY_FUNCTION__ << ltdb_board << "index = " << index << "." << std::endl;

      const bool succeed = ltdb_server->DefineFromOKS(index, enabled, LTDBs[i]);

      std::cout << __PRETTY_FUNCTION__ << ltdb_board
            << (succeed ? "Successfully added " : "Failed to add ")
            << "LTDB BOARD UID '" << LTDBs[i]->UID() << "'" 
                    << " [" << (enabled ? "Enabled" : "Disabled") << "] "
                    << "at slot #" << to_string(index) << "." << std::endl;

          std::cout << __PRETTY_FUNCTION__ << ltdb_board << "//////////////////////////////////////////////////////////////////////////////" << std::endl;
    }

	}
	catch (ers::Issue& ex) {
	    Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot configure OPC Rack", ex);
	    ers::error(issue);
	    return false;

	}

	return true;
}

bool LtdbConfig::Load_(void)
{
  return LoadDB();
}

bool LtdbConfig::LoadDB(void)
{
  if (!Rcc()->configuration()->loaded()) {
    Larg::rcc::NoConfigDbLoaded issue(ERS_HERE);
    ers::error(issue);

    return false;
  }

  bool rtn = true;

  const daq::core::Segment *segment = Rcc()->configuration()->get<daq::core::Segment> (Rcc()->GetSegmentName());

  if (segment->get_UsesObjects().size() > 0) {
    // --------------------------------------------------------------------
    // NOTA: Only ROD crate is referenced by the "UsesObjects" relationship
    // --------------------------------------------------------------------
    rtn = BuildRODC(Rcc()->configuration()->cast<Larg::dal::LARG_RODC, daq::core::HW_Object> (segment->get_UsesObjects()[0]));
  }

  return rtn;
}

bool LtdbConfig::MapFEBtoRODLinks(void)
{
  bool rtn = true;

  febToDSPMap_type::iterator it = myFebToDSPMap.begin();

  for (; it != myFebToDSPMap.end(); ++it) {
    OnlineObject *feb = OnlineObject::GetObject(it->first);

    if (feb) {
      const Larg::dal::LARG_DSP_Channel *channel = Rcc()->configuration()->get<Larg::dal::LARG_DSP_Channel> (it->second.first);
      rtn &= MapFEBtoRODLink(feb, channel);
    }
  }

  return rtn;
}

bool LtdbConfig::MapFEBtoRODLink(const std::string& name)
{
  bool rtn = true;

  febToDSPMap_type::iterator it = myFebToDSPMap.find(name);

  if (it != myFebToDSPMap.end()) {
    OnlineObject *feb = OnlineObject::GetObject(it->first);

    if (feb) {
      const Larg::dal::LARG_DSP_Channel *channel = Rcc()->configuration()->get<Larg::dal::LARG_DSP_Channel> (it->second.first);
      rtn &= MapFEBtoRODLink(feb, channel);
    }
  }

  return rtn;
}

bool LtdbConfig::MapFEBtoRODLink(OnlineObject *feb, const Larg::dal::LARG_DSP_Channel *channel)
{
  bool rtn = true;

  if (channel) {
    const Larg::dal::LARG_PU *pu = channel->get_PartOf();
    OnlineObject *rod            = OnlineObject::GetObject(pu->get_PartOf()->UID());

    if (rod) {
      unsigned short slot = (pu->get_Id() * 2) + channel->get_Id();

      if (feb)
        rtn &= dynamic_cast<Rod *> (rod)->SetFebID(feb, slot);
      else
        rtn &= dynamic_cast<Rod *> (rod)->ResetFebID(slot);
    }
  }

  return rtn;
}

bool LtdbConfig::SetDetectorIDsToHalfPUs(const std::string& name)
{
  if (!myFebToDSPMap.empty()) {
    febToDSPMap_type::iterator it;

    for (it = myFebToDSPMap.begin(); it != myFebToDSPMap.end(); ++it) {
      const Larg::dal::LARG_FE_Module *feb = Rcc()->configuration()->get<Larg::dal::LARG_FE_Module> (it->first);

      if (feb) {
        const Larg::dal::LARG_DSP_Channel *channel = Rcc()->configuration()->get<Larg::dal::LARG_DSP_Channel> (it->second.first);

        if (channel) {
          const Larg::dal::LARG_PU *pu = channel->get_PartOf();

          if (name.empty() || (name == pu->get_PartOf()->UID())) {
            OnlineObject *object = OnlineObject::GetObject(pu->get_PartOf()->UID());

            if (object) {
              Rod *rod       = dynamic_cast<Rod *> (object);
              HalfPu *halfPu = rod->GetSlice(pu->get_Id()).GetPu()->GetHalfPu(channel->get_Id());

              halfPu->RemoveAllDetectorIDs();
              halfPu->AddDetectorID(myFebToDetectorIDMap[feb->UID()]);
            }
          }
        }
      }
    }
  }

  return true;
}

bool LtdbConfig::SetRodDsp(const Larg::dal::LARG_Module *module,
                           OnlineObject *                object)
{
  const Larg::dal::LARG_IO_Module *ioModule = Rcc()->configuration()->cast<Larg::dal::LARG_IO_Module, Larg::dal::LARG_Module> (
    module);

  std::vector<const Larg::dal::LARG_PU *> PUs = GetPUs(ioModule);

  size_t nPu = PUs.size();

  if (PUs.size() > 4) {
    std::ostringstream oss;

    oss << "ROD module \"" << module->UID() << "\" have more than 4 PU";
    Larg::rcc::BadConfiguration issue(ERS_HERE, oss.str());
    ers::warning(issue);

    nPu = 4;
  }

  Rod *rod = dynamic_cast<Rod *> (object);

  if (rod) {
    try {
      Rod::DSP_state_type state(0UL);

      for (size_t ii = 0; ii < nPu; ++ii) {
        if (PUs[ii]->get_Contains().size() != 2) {
          std::ostringstream oss;

          oss << "ROD module \"" << module->UID() << " has bad number of DSP input channels (" << PUs[ii]->get_Contains().size() <<
          "/2)";
          Larg::rcc::BadConfiguration issue(ERS_HERE, oss.str());
          ers::error(issue);

          return false;
        }

        rod->GetSlice(PUs[ii]->get_Id()).GetPu()->SetBusyTimeout(PUs[ii]->get_BusyTimeout());

        const Larg::dal::LARG_DSP_Channel *channel1 =
          Rcc()->configuration()->cast<Larg::dal::LARG_DSP_Channel, daq::core::ResourceBase> (PUs[ii]->get_Contains()[0]);
        const Larg::dal::LARG_DSP_Channel *channel2 =
          Rcc()->configuration()->cast<Larg::dal::LARG_DSP_Channel, daq::core::ResourceBase> (PUs[ii]->get_Contains()[1]);

        if (!channel1 || !channel2) {
          std::ostringstream oss;

          oss << "PU \"" << PUs[ii]->UID() << "\" has some input channels different than type: " <<
          Larg::dal::LARG_DSP_Channel::s_class_name;
          Larg::rcc::BadConfiguration issue(ERS_HERE, oss.str());
          ers::error(issue);

          return false;
        }

        if (channel1->get_Id() == channel2->get_Id()) {
          std::ostringstream oss;

          oss << "DSP input channels \"" << channel1->UID() << "\" and \"" << channel2->UID() << "\" have same ID = " <<
          channel1->get_Id();
          Larg::rcc::BadConfiguration issue(ERS_HERE, oss.str());
          ers::error(issue);

          return false;
        }

        if (!PUs[ii]->get_State()) {
          Larg::rcc::ActionsInvalidated issue(ERS_HERE, "PU device", PUs[ii]->UID());
          ers::info(issue);
        }

        for (size_t jj = 0; jj < PUs[ii]->get_Contains().size(); ++jj) {
          const Larg::dal::LARG_DSP_Channel *channel =
            Rcc()->configuration()->cast<Larg::dal::LARG_DSP_Channel, daq::core::ResourceBase> (PUs[ii]->get_Contains()[jj]);

          if (PUs[ii]->get_State() && channel->get_State()) {
            state[(PUs[ii]->get_Id() * 2) + channel->get_Id()] = !IsResourceIsDisabled(channel);
          } else {
            state[(PUs[ii]->get_Id() * 2) + channel->get_Id()] = 0;

            if (!channel->get_State()) {
              Larg::rcc::ActionsInvalidated issue(ERS_HERE, "DSP input channel", channel->UID());
              ers::warning(issue);
            }
          }

          myRodChannelMap.insert(rodChannelMap_type::value_type(channel->UID(),
                                 std::make_pair((PUs[ii]->get_Id() * 2) + channel->get_Id(),
                                 object)));
        }
      }

      std::cout << __FUNCTION__ << ": set DSP state [" << state << "] to module "
                << rod->GetName() << "@" << rod->GetClassName() << std::endl;

      rod->SetDspState(state);
    }
    catch (ers::Issue& ex) {
      Larg::rcc::BadConfiguration issue(ERS_HERE, "Cannot set DSP state", ex);
      ers::error(issue);
      return false;
    }
  }

  return true;
}


bool LtdbConfig::UnloadDB(void)
{
  return true;
}

bool LtdbConfig::Unload_(void)
{
  return true;
}

// set auto-registration, used by Dynamic Class Loader
// NOTA: the macro use C function (i.e. use non-POD type).
// Replace 'const std::string&' by 'const char *'
ONLINE_DCL_RECORD_CLASS_2P(LtdbConfig, const char *, RunCtrlActions *)