/*
*  LtdbServer.cpp
*
* Created on: May 23, 2020
* Author: D. Panchal
*
*/
#include <string>
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>
#include <dal/util.h>

#include "LTDB/Ltdb.h"
#include "LTDB/LtdbServer.h"

/**
* Constructor which creates an instance of the LtdbServer which is a container of several Ltdbs.
* @param dummy integer needed to when called by the configuration plugin
* @param boardName unused but will be the name of this "rack"
*/
LtdbServer::LtdbServer(const uint32_t dummy, const std::string& boardName)
{
    LOG("Creating instance of LtdbServer.");
    REGISTER_CLASS(this);
}

/**
* Destructor which deletes the instance of LtdbServer and clears the vector of Ltdbs.
*/
LtdbServer::~LtdbServer()
{
    myLtdbs.clear();
}

//////////////////////////// Setter methods ////////////////////////////
/**
* Set the OPC peripheral server address serviced by the rack.
* @param server_address server address
*/
void LtdbServer::setServerAddress(const std::string& server_address)
{
    myServerAddress = server_address;
}

/**
* Set the length of the vector containing the Ltdbs.
* @param num_ltdbs number of Ltdbs
*/
void LtdbServer::setNumLtdbs(const uint16_t num_ltdbs)
{
    numLtdbs = num_ltdbs;
}

/**
* Set the server ID.
* @param serverID 
*/
void LtdbServer::setServerId(const std::string& serverID)
{
    myServerId = serverID;
}

/**
* Create a vector containing LTDB objects.
*/
void LtdbServer::CreateLTDBs()
{
    for (size_t i = 0; i < numLtdbs; ++i) {
        Ltdb* ltdb = new Ltdb(i, "random_node_"+std::to_string(i));
        myLtdbs.emplace_back(ltdb);
    }
}
////////////////////////////////////////////////////////////////////////


//////////////////////////// Getter methods ////////////////////////////
/**
* Get the OPC peripheral server address.
* @return server address
*/
const std::string LtdbServer::getServerAddress()
{
  return myServerAddress;
}

/**
* Get the length of the vector containing the LTDB objects.
* @return number of ltdbs handled by the class instance
*/
const uint16_t LtdbServer::getNumLtdbs()
{
  return numLtdbs;
}

/**
* Get the server ID.
* @return serverID 
*/
const std::string LtdbServer::getServerId()
{
  return myServerId;
}

////////////////////////////////////////////////////////////////////////

/**
* Method to set the class variables from OKS file. 
* In addition to the class variables for this class, the variables for LTDBs handled by this class are also initialized.
* @param slotID index of the LTDB board within the LTDB rack
* @param enabled whether the LTDB is enabled or not
* @param params parameters for the LTDBs read from the OKS file
* @return true if the class variables were successfully initialized, otherwise false
*/
bool LtdbServer::DefineFromOKS(const uint32_t slotID, const bool enabled, const Larg::dal::LARG_LTDB_HW*& params)
{
    if (params == NULL) {
        const std::string& msg = "Received NULL value for parameter 'params'.";
        LOG(msg);
        return false;
    }

  if (enabled == false) {
    return true;
  }
  LOG("-------------- Setting up OKS parameters --------------");

  return myLtdbs[slotID]->DefineFromOKS(params);

}

//////////////////////////// TDAQ FSM implementations ////////////////////////////
/**
* Init FSM which which calls the Init method for each LTDB.
* @return true if all LTDBs are initialized, otherwise false
*/
bool LtdbServer::Init_()
{

    LOG("-------------- Initializing the LTDB nodes --------------");
    for (auto &ltdb: myLtdbs) {
        ltdb->setServerAddress(myServerAddress);
        if (!ltdb->Init()) {
            LOG("Could not initialize LTDB");
            return false;
        }

    }

    return true;
}

/**
* Load FSM which which calls the Load method for each LTDB.
* @return true if all LTDBs are loaded, otherwise false
*/
bool LtdbServer::Load_()
{

    LOG("-------------- Loading the LTDB nodes --------------");
    for (auto &ltdb: myLtdbs) {
        if (!ltdb->Load()) {
            LOG("Could not load LTDB");
            return false;
        }

    }

    return true;
}

/**
* Unload FSM which which calls the Unload method for each LTDB.
* @return true if all LTDBs are unloaded, otherwise false
*/
bool LtdbServer::Unload_()
{

    LOG("-------------- Unloading the LTDB nodes --------------");
    for (auto &ltdb: myLtdbs) {
        if (!ltdb->Unload()) {
            LOG("Could not unload LTDB");
            return false;
        }

    }   

    return true;
}

/**
* Config FSM which which calls the Config method for each LTDB.
* @return true if all LTDBs are configured, otherwise false
*/
bool LtdbServer::Config_()
{

    LOG("-------------- Configuring the LTDB nodes --------------");
    for (auto &ltdb: myLtdbs) {
        if (!ltdb->Config()) {
            LOG("Could not configure LTDB");
            return false;
        }

    }   

    return true;
}

/**
* Unconfig FSM which which calls the Unconfig method for each LTDB.
* @return true if all LTDBs are unconfigured, otherwise false
*/
bool LtdbServer::Unconfig_()
{

    LOG("-------------- Unconfiguring the LTDB nodes --------------");
    for (auto &ltdb: myLtdbs) {
        if (!ltdb->Unconfig()) {
            LOG("Could not unconfigure LTDB");
            return false;
        }

    }   

    return true;
}

/**
* Start FSM which which calls the Start method for each LTDB.
* @return true 
*/
bool LtdbServer::Start_()
{
  LOG("Calling start function.");
  return true;
}

/**
* Stop FSM which which calls the Stop method for each LTDB.
* @return true 
*/
bool LtdbServer::Stop_()
{
  LOG("Calling stop function.");
  return true;
}

/**
* PrepareForRun FSM which which calls the PrepareForRun method for each LTDB.
* @return true 
*/
bool LtdbServer::PrepareForRun_()
{
  LOG("Calling prepare for run function.");
  return true;
}
////////////////////////////////////////////////////////////////////////

/**
* Internal logging facility called using LOG() macro
*/
void LtdbServer::Log(const std::string& pretty, const std::string& details)
{
    // Try to get current time at TDAQ::ERS default format
  std::string time = "UNKNOWN TIME";
  struct timeval tp;
  if (!gettimeofday(&tp, NULL)) {
    // YYYY-Mon-dd hh:mm:ss
    struct tm* timeinfo = localtime(&tp.tv_sec);
    char buffer[32];
    size_t s = strftime(buffer, sizeof (buffer), "%G-%h-%d %T", timeinfo); // Format: 2020-Mar-06 07:21:39
    if (s < sizeof (buffer)) {
      // Milliseconds on 3 digits
      std::stringstream stream;
      stream << std::setfill('0') << std::setw(3) << std::dec << std::to_string(tp.tv_usec / 1000);
      time = std::string(buffer) + "," + stream.str();
    }
  }

  std::cout << time + " - [LTDB-SERVER_" + getServerId() + "] - " + pretty + (details.length() > 0 ? " : " + details : "") << std::endl;

}

// set auto-registration, used by Dynamic Class Loader
// NOTA: the macro use C function (i.e. use non-POD type).
// Replace 'const string&' by 'const char *'
ONLINE_DCL_RECORD_CLASS_2P(LtdbServer, const uint32_t, const char *)
