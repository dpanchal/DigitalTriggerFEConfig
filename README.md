# LAr Trigger Digitizer Board (LTDB) management software package

## Introduction

Package to build the LTDB segment in the LArgOnline framework. 

This repository contains a classes which define the software description of an LTDB, a communication client, and a server which manages multiple LTDBs. 
It also contains the required XML files used to set up a segment, define the software repository, and the schema for the hardware.

## Required LArgOnline dependencies

`Base`
`RodCrate`

## How to setup the LArgOnline environment?

1. Please follow this [TWiki](https://atlasop.cern.ch/twiki/bin/view/LAr/LargOnlineDevelopment#Development_in_the_LargOnline_Fr) and follow the instructions on how to setup the 
LArgOnline environment. 

For this package, we need `LargOnline-831-42-01` and will be refered to as `<LargOnlineVersion>`.

2. Once you have setup the environment, you need to install the correct versions for the required dependencies. One can find the correct release for a dependency by searching for
the version number in 

`/afs/cern.ch/atlas/project/LargOnline/releases/<LargOnlineVersion>/<LargOnlineDependency>/`

3. Install and make the required dependencies using the `getpkg` command 

```getpkg -t <dependency_version_number> <LargOnlineDependency> -g```

The `-g` flag ensures a buildsystem is generated using CMake. If the build is successful, you can build the dependency (not required).

## How to build the package?

1. Clone the repository

In your LargOnline devarea, clone the package.

```git clone <your_prefered_clone_option>```

2. Go in the repository directory and create a folder `build` and `cd` into the build directory. 

3. Build the package

```cmake ..``` 

```make -j4 && make install```

If everything works well, you have successfully built the package! 

# Package descripton

## Scripts

**ClientSession**

A lightweight communication class which connects to the OPC-UA peripheral server using `UaSession` SDK. This class sets up a connection and addresses a server node to
execute a method.

**Ltdb**

Software description of the physical LTDB hardware. This class implements the low-level methods for each TDAQ state transition. The communication between the peripheral
server and the class instance is managed by an instance of _ClientSession_.

**LtdbClient** 

Class which manages state of several _Ltdb_ class instances. Implements wrapper functions for each TDAQ state transition to execute the state method serially
for each _Ltdb_ managed by the server.

**LtdbConfig**

Class which handles the configuration of the hardware objects defined in the OKS database. The class is built as a `LARG_Plugin` which is loaded by `RunCrateCtrlActions`.
When a ROD Crate object is built, the objects used by ROD Crate are built, wherein the `LtdbConfig::SetLtdbClient` method is called. In this method, the `Ltdb::DefineFromOKS` sets
up the variables required by the LTDB hardware object.

## XML files

**Larg.LTDB.sw.data.xml**

The software XML file defines the run control application binary. 

**Larg.LTDB.hw.data.xml**

The hardware XML file defines configuration of LTDB hardware objects, LTDB server and a dummy ROD crate.

**Larg.LTDB.schema.data.xml**

The schema file describes the definition of the hardware objects. 

**Larg.segments.data.xml**

The segments file defines the segement _LTDBSegment_.

**Larg.plugins.data.xml**

The plugins file defines any plugins used by the partition. We use three plugins for this package, two `LARG_Plugins` and one `LARG_Config` plugin.
The `Ltdb`, `LtdbClient`, and its DAL are used as `LARG_Plugins`. The `LtdbConfig` library is used as a configuration plugin.

**Larg.LTDB.repository.xml**

The repository file sets up the software repositories.